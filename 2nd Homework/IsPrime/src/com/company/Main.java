package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        if( isPrime(n) )
            System.out.println( n + " is prime "  );
        else
            System.out.println( n + " is not prime "  );
    }
    public static  boolean isPrime(int n){
        if( n <= 1 ) return  false;
        int limit = (int) Math.sqrt( (double) n );
        for( int i = 2 ; i <= limit ; ++i ){
            if( n % i == 0 ) return  false;
        }
        return  true;
    }


}
