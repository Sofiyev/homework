package com.itextpdf;

import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Paragraph;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by NurlanSofiyev on 3/1/2017.
 */
public class jumpstartHelloWorld {
    public static final String DIR = "pdfs/Nurlan.pdf";
    public static void main(String[] args) throws IOException {

        File file = new File(DIR);
        file.getParentFile().mkdirs();
        FileOutputStream fileOutputStream = new FileOutputStream(DIR);
        PdfWriter pdfWriter = new PdfWriter(fileOutputStream);
        PdfDocument pdfDocument = new PdfDocument(pdfWriter);
        Document document = new Document(pdfDocument);
        document.add(new Paragraph("Hello World"));
        document.close();

    }



}
