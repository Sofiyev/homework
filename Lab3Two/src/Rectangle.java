
public class Rectangle extends Shape
{

    private double width ;
    private double height ;
    public Rectangle () {
        super() ;
        width=1 ;
        height=1 ;

    }

    public Rectangle(double width,double height) {

        this.width=width;
        this.height=height ;

    }

    public Rectangle (double xPos,double yPos,double width,double height){
        super(xPos,yPos) ;
        this.width = width;
        this.height = height;


    }






    @Override
    public String toString() {
        return ("Rectangle ("+getXPos()+","+getYPos()+")-("+( getXPos()+width)+","+(getYPos()+height)+")") ;

    }

    @Override
    public double getArea() {
        return width*height ;

    }

    @Override
    public void printShape() {
        for(int x=0 ;x<height ;x++){
            for(int y=0 ;y<width;y++){
                System.out.print("*");
            }
            System.out.println("");
        }
    }






}