
public class Circle  extends Shape
{

    private double radius ;



    public Circle(){
        super() ;
        radius = 1.0 ;
    }


    public Circle (double radius ) {
        super() ;
        this.radius = radius ;
    }
    public Circle(double x ,double y,double radius) {
        super(x,y) ;
        this.radius = radius ;
    }

    @Override
    public double getArea() {
        return (Math.PI*radius*radius) ;
    }

    @Override
    public void printShape() {
        System.out.print("0");
    }
    @Override
    public String toString() {
        return ("Circle ("+getXPos()+", "+getYPos()+"),"+radius+".") ;
    }

}