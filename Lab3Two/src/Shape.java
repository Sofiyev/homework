

public abstract  class Shape {

    private double xPos ;
    private double yPos ;
    public Shape(){
        xPos=0 ;
        yPos=0 ;



    }
    public Shape (double xPos,double yPos) {
        this.xPos=xPos ;
        this.yPos=yPos ;
    }



    public double getXPos() {
        return xPos ;
    }
    public double getYPos() {

        return yPos ;

    }
    public void setPos(double x ,double y) {
        yPos=y ;
        xPos=x ;
    }

    @Override
    public String toString() {

        return ("Shape ("+xPos+","+yPos+" )") ;
    }
    abstract public double getArea() ;
    abstract public void printShape();


}