/**
 * Created by sofiyev on 3/13/17.
 */

public class Student {
    private String name;
    private String major;
    private String email;
    private int nCredits;
    private double GPA;

    public void addTocredits(int c){
        nCredits = nCredits + c;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getnCredits() {
        return nCredits;
    }

    public void setnCredits(int nCredits) {
        this.nCredits = nCredits;
    }

    public double getGPA() {
        return GPA;
    }

    public void setGPA(double GPA) {
        this.GPA = GPA;
    }



}
