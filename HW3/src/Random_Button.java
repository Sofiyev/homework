import javax.swing.*;
import java.util.Random;

/**
 * Created by sofiyev on 3/13/17.
 */
public class Random_Button {
    private JButton generateButton;
    private JLabel number;
    private JPanel panel;

    public Random_Button() {
        generateButton.addActionListener( e -> {
            Random random = new Random();
            number.setText( Integer.toString(  random.nextInt(100) + 1 )   );
        } );
    }

    public static void main(String[] args){
        JFrame frame = new JFrame("Random Number Generator mod 100");
        frame.setContentPane(new Random_Button().panel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);

    }

}
