/**
 * Created by sofiyev on 3/13/17.
 */
public class Dog {

    public Dog(String name, int age){
        this.name = name;
        this.age = age;
    }

    private String name;
    private int age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
    public int calculateAge(int age){
        return 7*age;
    }

    @Override
    public String toString() {
        return " Dog name is " + name + ". Age : " + Integer.toString(  calculateAge(age) );
    }
}
