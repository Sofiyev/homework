
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 * Created by sofiyev on 4/13/17.
 */

public class SQL_CONNECT {


    private static Connection connection = null;
    public static void main(String[] args) throws SQLException {

        connection = getDBConnection();

        if (connection != null) {
            System.out.println("Success !");
        } else {
            System.out.println("Failed !");
            return;
        }


        createTable();
        insertIntoTable("nurlan", "sofiyev");
        connection.close();


    }


    private static void createTable() throws SQLException {

        PreparedStatement preparedStatement = null;

        String createTableSQL = "CREATE TABLE USER("
                + "USER_ID int NOT NULL AUTO_INCREMENT, "
                + "FirstName VARCHAR(35) NOT NULL, "
                + "Surname VARCHAR(35) NOT NULL, "
                + "TimeAdded DATE, " + "PRIMARY KEY (USER_ID) "
                + ")";

        try {

            preparedStatement = connection.prepareStatement(createTableSQL);

            System.out.println(createTableSQL);

            preparedStatement.executeUpdate();

            System.out.println("Table \"database\" is created!");

        } catch (SQLException e) {

            System.out.println(e.getMessage());

        } finally {

            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }

    }

    private static Connection getDBConnection() {
        Connection connection = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException ex) {
            System.out.println("MySQL JDBC Driver not found!");
            ex.printStackTrace();
            return null;
        }

        System.out.println("MySQL JDBC Driver Registered!");

        try {
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/" + "test", "root", "388758823");
        } catch (SQLException ex) {
            System.out.println("Connection Failed ! ");
            System.out.println(ex.toString());
            //ex.printStackTrace();
            return null;
        }


        return connection;
    }

    private static void insertIntoTable(String firstname, String surname) throws SQLException {

        PreparedStatement preparedStatement = null;

        String insertTableSQL = "INSERT INTO USER"
                + "(FirstName, Surname, BIRTHDAY) VALUES"
                + "(?,?,?)";

        try {
            preparedStatement = connection.prepareStatement(insertTableSQL);

            preparedStatement.setString(1, firstname);
            preparedStatement.setString(2, surname);
            preparedStatement.setTimestamp(3, new Timestamp(new Date().getTime()));


            preparedStatement.executeUpdate();

            System.out.println("User '" + firstname + "' is inserted ! ");

        } catch (SQLException e) {

            System.out.println(e.getMessage());

        } finally {

            if (preparedStatement != null) {
                preparedStatement.close();
            }

        }

    }

}