
/**
 * Created by sofiyev on 4/14/17.
 */


import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import java.io.*;
import java.util.Scanner;


public class Main{

    public static void main(String args[]) throws Exception {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter a path to a file: ");

        File file = new File( scanner.nextLine() );
        POIFSFileSystem poifsFileSystem = new POIFSFileSystem(new FileInputStream(file));
        HSSFWorkbook workbook = new HSSFWorkbook(poifsFileSystem);
        HSSFSheet sheet = workbook.getSheetAt(0);


        int rows = sheet.getPhysicalNumberOfRows();
        int columbs = sheet.getDefaultColumnWidth();

        for( int r = 0 ; r < rows ; ++r ){
            HSSFRow row =  sheet.getRow(r);
            if( row != null ){
                for( int c = 0 ; c < columbs; ++c ){
                    HSSFCell cell = row.getCell( c );
                    if( cell != null ){
                        System.out.println(cell);
                    }
                }
            }
        }



    }
}